/* 学習モデル */
var Model = (function() {
    /* コンストラクタ */
    var Model = function(a, b, k) {
        this.alpha = a; //ラグランジュ係数の初期値
        this.b = b;
        this.kernel = k;    //カーネル関数
    }

    var pt = Model.prototype;

    pt.judge = function (x, Data) {
        return step(this.dist(x, Data));
    }
    /* 識別関数
    * x:データ
    * X:訓練データ集合
    * Y:訓練データラベル集合
    */
    pt.dist = function(x, Data) {
        var ret = 0;
        for (var i=0; i < Data.length; ++i) {
            ret += this.alpha[i] * Data[i].y * this.kernel(x, Data[i].x);
        }
        ret -= this.b;

        return ret;
    }

    pt.distWitoutBias = function (x, Data) {
        var ret = 0;
        for (var i=0; i < Data.length; ++i) {
            ret += this.alpha[i] * Data[i].y * this.kernel(x, Data[i].x);
        }

        return ret;
    }

    return Model;
})();

function step(x) {
    if (x >= 0) {
        return 1;
    } else {
        return -1;
    }
}

function phi(x, i) {
    var x_added = [1].concat(x);
    return x_added[i];
}

function linearKernel(x1, x2) {
    var ret = 0;
    for (var i=0; i < x1.length; ++i) {
        ret += x1[i] * x2[i];
    }
    return ret;
}

function rbfKernelGenerator(s) {
    var sigma = s;
    return (function (x1, x2) {
        return Math.exp(-l2norm(vectorSubtraction(x1, x2)) / (2 * Math.pow(sigma, 2)));
    });
}

function polynomialKernelGenerator(c, p) {
    var c = c;
    var p = p;
    return (function (x1, x2) {
        return Math.pow(linearKernel(x1, x2) + c, p);
    });
}

function l2norm(x) {
    var ret = 0;
    for (var i=0; i < x.length; ++i) {
        ret += Math.pow(x[i], 2);
    }
    return Math.sqrt(ret);
}

function vectorSubtraction(x1, x2) {
    var x = [];
    for (var i=0; i < x1.length; ++i) {
        x[i] = x1[i] - x2[i];
    }
    return x;
}

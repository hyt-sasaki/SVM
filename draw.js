/* 点の描画
* d: データ
* canvas: canvasオブジェクト
* r: 座標系の定義幅
*/
function drawPoint(d, canvas, r, tLabelColor, fLabelColor) {
    var ctx = canvas.getContext('2d');
    var c = dataToCanvas(d.x, canvas, r);

    ctx.beginPath();
    if (d.y === 1) {
        ctx.strokeStyle = tLabelColor;
    } else {
        ctx.strokeStyle = fLabelColor;
    }
    ctx.arc(c[0], c[1], canvas.width / 125, 0, Math.PI * 2, true);
    ctx.fill();
    ctx.stroke();
    ctx.strokeStyle = 'rgb(0, 0, 0)';
}

/* 点群の描画
* Data: データ集合
* canvas: canvasオブジェクト
* r: 座標系の定義幅
*/
function drawPoints(Data, canvas, r, tLabelColor, fLabelColor) {
    for (var i=0; i < Data.length; ++i) {
        var d = Data[i];
        drawPoint(d, canvas, r, tLabelColor, fLabelColor);
    }
}

/* サポートベクタ群の描画
* Data: サポートベクタ集合
* canvas: canvasオブジェクト
* r: 座標系の定義幅
*/
function drawSupportVectors(Data, canvas, r, tLabelColor, fLabelColor) {
    for (var i=0; i < Data.length; ++i) {
        var d = Data[i];
        drawSupportVector(d, canvas, r, tLabelColor, fLabelColor);
    }
}

/* 点の描画
* d: サポートベクタ
* canvas: canvasオブジェクト
* r: 座標系の定義幅
*/
function drawSupportVector (d, canvas, r, tLabelColor, fLabelColor) {
    var ctx = canvas.getContext('2d');
    var c = dataToCanvas(d.x, canvas, r);

    ctx.beginPath();
    if (d.y === 1) {
        ctx.strokeStyle = tLabelColor;
    } else {
        ctx.strokeStyle = fLabelColor;
    }
    ctx.lineWidth = 3;
    ctx.arc(c[0], c[1], canvas.width / 100, 0, Math.PI * 2, true);
    ctx.stroke();
    ctx.strokeStyle = 'rgb(0, 0, 0)';
    ctx.lineWidth = 1;
}

/* 関数の描画
* m: 関数のモデル
* canvas: canvasオブジェクト
* r: 座標系の定義幅
* color: 描画する曲線の色
*/
function drawFunction(m, canvas, r, color) {
    var ctx = canvas.getContext('2d');
    ctx.strokeStyle = color;

    var N = 100;

    ctx.beginPath();
    var x0 = [-r[0] / 2, m.h(-r[0] / 2)];
    var c0 = dataToCanvas(x0, canvas, r);
    ctx.moveTo(c0[0], c0[1]);
    for (var i=1; i <= N; ++i) {
        var x = [-r[0] / 2 + r[0] / N * i, m.h(-r[0] / 2 + r[0] / N * i)];

        var c = dataToCanvas(x, canvas, r);

        ctx.lineTo(c[0], c[1]);
    }
    ctx.stroke();
    ctx.strokeStyle = 'rgb(0, 0, 0)';
}

/* 識別領域の描画
*
*/
function drawDiscriminant(Data, canvas, r, m, tLabelColor, fLabelColor) {
    var xNum = 100;
    var yNum = 100;
    var rectWidth = canvas.width / xNum;
    var rectHeight = canvas.height / yNum;
    for (var i=0; i < xNum; ++i) {
        for (var j=0; j < yNum; ++j) {
            var rectLeft = rectWidth * i;
            var rectTop = rectHeight * j;
            var c = getRectCenter(rectWidth, rectHeight, rectLeft, rectTop);
            var x = canvasToData(c, canvas, r);
            var color;
            if (m.judge(x, Data) === 1) {
                color = tLabelColor;
            } else {
                color = fLabelColor;
            }
            drawCross(rectWidth, rectHeight, rectLeft, rectTop, canvas, color);
        }
    }
}

function drawCross(w, h, l, t, canvas, color) {
    /* バツ印の左上座標 */
    var x1 = l;
    var y1 = t;
    /* バツ印の右下座標 */
    var x2 = l + w;
    var y2 = t + h;
    /* バツ印の右上座標 */
    var x3 = l + w;
    var y3 = t;
    /* バツ印の左下座標 */
    var x4 = l;
    var y4 = t + h;

    var ctx = canvas.getContext('2d');

    ctx.beginPath();
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.moveTo(x3, y3);
    ctx.lineTo(x4, y4);
    ctx.strokeStyle = color;
    ctx.stroke();
    ctx.strokeStyle = 'rgb(0, 0, 0)';
}

function getRectCenter(w, h, l, t) {
    return [l + w / 2, t + h / 2];
}

/* 座標軸の描画
* canvas: canvasオブジェクト
* r: 座標系の定義幅
*/
function drawAxes(canvas, r) {
    var ctx = canvas.getContext('2d');

    ctx.beginPath();
    /* x軸の描画 */
    ctx.moveTo(0, canvas.height / 2);
    ctx.lineTo(canvas.width, canvas.height / 2);
    ctx.moveTo(canvas.width, canvas.height / 2);
    ctx.lineTo(canvas.width * 108 / 110, canvas.height * 53 / 110);
    ctx.lineTo(canvas.width * 108 / 110, canvas.height * 57 / 110);
    ctx.closePath();
    ctx.moveTo(canvas.width * 105 / 110, canvas.height * 54 / 110);
    ctx.lineTo(canvas.width * 105 / 110, canvas.height * 56 / 110);
    ctx.moveTo(canvas.width * 5 / 110, canvas.height * 54 / 110);
    ctx.lineTo(canvas.width * 5 / 110, canvas.height * 56 / 110);

    /* y軸の描画 */
    ctx.moveTo(canvas.width / 2, canvas.height);
    ctx.lineTo(canvas.width / 2, 0);
    ctx.moveTo(canvas.width / 2, 0);
    ctx.lineTo(canvas.width * 53 / 110, canvas.height * 2 / 110);
    ctx.lineTo(canvas.width * 57 / 110, canvas.height * 2 / 110);
    ctx.closePath();
    ctx.moveTo(canvas.width * 54 / 110, canvas.height * 5 / 110);
    ctx.lineTo(canvas.width * 56 / 110, canvas.height * 5 / 110);
    ctx.moveTo(canvas.width * 54 / 110, canvas.height * 105 / 110);
    ctx.lineTo(canvas.width * 56 / 110, canvas.height * 105 / 110);

    ctx.stroke();
    ctx.fill();

    /* 文字の描画 */
    var normalSize = Math.min(canvas.width, canvas.height) / 20;
    var subscriptSize = Math.floor(normalSize * 0.5);
    var fontname = String(normalSize) + "px 'Times New Roman'";
    var _fontname = String(subscriptSize) + "px 'Times New Roman'";
    var fontnameItalic = "Italic " + fontname
    var _fontnameItalic = "Italic " + _fontname

    ctx.font = fontnameItalic;
    ctx.textAlign = 'left';
    ctx.textBaseline = 'middle';
    ctx.fillText('x', canvas.width * 58 / 110, canvas.height * 2 / 110);
    ctx.font = _fontname;
    ctx.textBaseline = 'bottom';
    var metrics = ctx.measureText('x');
    ctx.fillText('2', canvas.width * 58 / 110 + metrics.width * 1.5, (canvas.height * 2 / 110) + normalSize / 2);
    ctx.textAlign = 'right';
    ctx.textBaseline = 'middle';
    ctx.font = fontname;
    ctx.fillText(String(r[0] / 2), canvas.width * 52 / 110, canvas.height * 5 / 110);
    ctx.fillText('-' + String(r[0] / 2), canvas.width * 52 / 110, canvas.height * 105 / 110);

    ctx.font = fontnameItalic;
    ctx.textAlign = 'center';
    ctx.textBaseline = 'bottom';
    ctx.fillText('x', canvas.width * 108 / 110, canvas.height * 52 / 110);
    ctx.font = _fontname;
    ctx.fillText('1', canvas.width * 108 / 110 + metrics.width * 1.1, (canvas.height * 52 / 110));

    ctx.font = fontname;
    ctx.textAlign = 'center';
    ctx.textBaseline = 'top';
    ctx.fillText(String(r[1] / 2), canvas.width * 105 / 110, canvas.height * 58 / 110);
    ctx.fillText('-' + String(r[1] / 2), canvas.width * 5 / 110, canvas.height * 58 / 110);
    ctx.textAlign = 'right';
    ctx.textBaseline = 'top';
    ctx.fillText('0', canvas.width * 54 / 110, canvas.height * 56 / 110);
}

/* 座標系の変換 */
function dataToCanvas(x, canvas, r) {
    var cx = canvas.width * (1 / 2 + x[0] / r[0]);
    var cy = canvas.height * (1 / 2 - x[1] / r[1]);
    return [cx, cy];
}

function canvasToData(c, canvas, r) {
    x = r[0] * (c[0] / canvas.width - 1 / 2);
    y = r[1] * (-c[1] / canvas.height + 1 / 2);
    return [x, y];
}

var SVM = (function () {
    /* コンストラクタ */
    var SVM = function (m, C) {
        this.model = m; //学習モデル
        this.C = C;     //ソフトマージンのペナルティパラメータ
        this.tol = 0.001;
        this.eps = 0.001;
    }

    var pt = SVM.prototype;

    /* 学習 */
    pt.train = function (Data) {
        var numChanged = 0;
        var examineAll = true;
        this.E = [];
        for (var i=0; i < Data.length; ++i) {
            this.E.push(-Data[i].y);
        }
        while(numChanged > 0 || examineAll) {
            numChanged = 0;
            if (examineAll) {
                for (var i=0; i < Data.length; ++i) {
                    numChanged += this.examineExample(i, Data);
                }
            } else {
                for (var i=0; i < Data.length; ++i) {
                    if (this.model.alpha[i] !== 0 && this.model.alpha[i] !== this.C) {
                        numChanged += this.examineExample(i, Data);
                    }
                }
            }
            if (examineAll) {
                examineAll = false;
            } else if (numChanged === 0) {
                examineAll = true;
            }
        }
    }

    /* エラーチェック */
    pt.check = function (Data) {
        var error = Data.length;
        for (var i=0; i < Data.length; ++i) {
            if (this.model.judge(Data[i].x, Data) === Data[i].y) {
                error--;
            }
        }
        return error;
    }

    pt.examineExample = function (i2, Data) {
        var y2 = Data[i2].y;
        var alpha2 = this.model.alpha[i2];
        //var E2 = this.model.dist(y2, Data) - y2;
        var E2 = this.E[i2];
        var r2 = E2 * y2;
        if ((r2 < - this.tol && alpha2 < this.C) || (r2 > this.tol && alpha2 > 0)) {
            var num = 0;
            for (var i=0; i < this.model.alpha.length; ++i) {
                var a = this.model.alpha[i];
                if (a !== 0 && a !== this.C) {
                    num++;
                }
            }
            if (num > 1) {
                var i1 = this.selectAlpha1(i2, Data);
                if (i1 !== -1) {
                    if (this.takeStep(i1, i2, Data)) {
                        return 1;
                    }
                }
            }
            var non_zero_C_alphas_idx = [];
            for (var i=0; i < this.model.alpha.length; ++i) {
                var a = this.model.alpha[i];
                if (a !== 0 && a !== this.C) {
                    non_zero_C_alphas_idx.push(i);
                }
            }
            while(non_zero_C_alphas_idx.length > 0) {
                var rnd = Math.floor(Math.random() * non_zero_C_alphas_idx.length);
                var i1 = non_zero_C_alphas_idx[rnd];
                if (this.takeStep(i1, i2, Data)) {
                    return 1;
                }
                non_zero_C_alphas_idx.splice(rnd, 1);
            }
            var all_idx = [];
            for (var i=0; i < this.model.alpha.length; ++i) {
                all_idx.push(i);
            }
            while(all_idx.length > 0) {
                var rnd = Math.floor(Math.random() * all_idx.length);
                var i1 = all_idx[rnd];
                if (this.takeStep(i1, i2, Data)) {
                    return 1;
                }
                all_idx.splice(rnd, 1);
            }
        }
        return 0;
    }

    pt.selectAlpha1 = function (i2, Data) {
        var E2 = this.E[i2];
        var maxDeltaE = 0;
        var maxI1 = -1;

        for (var i=0; i < this.model.alpha.length; ++i) {
            var E1 = this.model.dist(Data[i].x, Data) - Data[i].y;
            var deltaE = Math.abs(E1 - E2);
            if (deltaE > maxDeltaE) {
                maxDeltaE = deltaE;
                maxI1 = i;
            }
        }
        return maxI1;
    }

    pt.takeStep = function (i1, i2, Data) {
        if (i1 === i2) {
            return false;
        }
        var alpha1 = this.model.alpha[i1];
        var alpha2 = this.model.alpha[i2];
        var y1 = Data[i1].y;
        var y2 = Data[i2].y;
        var E1 = this.E[i1];
        var E2 = this.E[i2];
        var s = y1 * y2;
        var range = this.calcRange(i1, i2, Data);
        var L = range.L;
        var H = range.H;
        //if (H - L < 1e-8) {
        //    return false;
        //}
        if (L === H) {
            return false;
        }
        var k11 = this.model.kernel(Data[i1].x, Data[i1].x);
        var k12 = this.model.kernel(Data[i1].x, Data[i2].x);
        var k22 = this.model.kernel(Data[i2].x, Data[i2].x);
        var eta = 2 * k12 - k11 - k22;
        var a2 = alpha2 - y2 * (E1 - E2) / eta;
        if (a2 < L) {
            a2 = L;
        } else if (a2 > H) {
            a2 = H;
        }

        if (a2 < 1e-8) {
            a2 = 0;
        } else if (a2 > this.C - 1e-8) {
            a2 = this.C;
        }
        if (Math.abs(a2 - alpha2) < this.eps * (a2 + alpha2 + this.eps)) {
            return false;
        }
        var a1 = alpha1 + s * (alpha2 - a2);

        var bias = this.model.b;
        var b1 = E1 + y1 * (a1 - alpha1) * k11 + y2 * (a2 - alpha2) * k12 + bias;
        var b2 = E2 + y1 * (a1 - alpha1) * k12 + y2 * (a2 - alpha2) * k22 + bias;
        var b = (b1 + b2) / 2;
        this.model.b = b;

        for (var i=0; i < this.E.length; ++i) {
            this.E[i] = this.E[i] + y1 * (a1 - alpha1) * this.model.kernel(Data[i1].x, Data[i].x) + y2 * (a2 - alpha2) * this.model.kernel(Data[i2].x, Data[i].x) + bias - b;
        }

        this.model.alpha[i1] = a1;
        this.model.alpha[i2] = a2;

        return true;
    }

    pt.calcRange = function (i1, i2, Data) {
        var y1 = Data[i1].y;
        var y2 = Data[i2].y;
        var alpha_1 = this.model.alpha[i1];
        var alpha_2 = this.model.alpha[i2];
        var L, H;
        if (y1 !== y2) {
            L = Math.max(0, alpha_2 - alpha_1);
            H = Math.min(this.C, this.C + alpha_2 - alpha_1);
        } else {
            L = Math.max(0, alpha_1 + alpha_2 - this.C);
            H = Math.min(this.C, alpha_1 + alpha_2);
        }
        return {L:L, H:H};
    }

    pt.getSupportVectors = function (Data) {
        var ret = [];
        for (var i=0; i < this.model.alpha.length; ++i) {
            if (this.model.alpha[i] > 1e-5) {
                ret.push(Data[i]);
            }
        }
        return ret;
    }

    return SVM;
})();
